"""
Las condiciones iniciales o de referencia son:
    - El avión experimenta un vuelo estacionario, rectilineo y con alas a nivel
"""

import numpy as np

"""
Altura inicial de vuelo en pies
"""
hi=30000 #ft
#1 ft = 0,3048 m

h=hi*0.3048

print(hi*0.3048,'m')

"""
Velocidad inicial de vuelo nudos
"""
vi=180 #kt
#1 kt = 0,514444 m/s

vi*0.5144444445

print(vi*0.514444445,'m/s')


import sys
sys.path.insert(0, '../lib/')
import ISA as atmos
import density_and_dynamic_pressure as Ddp
import isentropic_gas as gas

T = atmos.T_isa(0)
print(T)

