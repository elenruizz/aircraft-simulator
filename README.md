## Proyecto Fin de Grado 
# SIMULADOR DE MANIOBRAS PARA VEHÍCULO AÉREO

Realizado por: Elena Sánchez Ruiz-Valdepeñas

Tutor academico: Marcos Antonio Rodríguez Jiménez

# Introducción

La mecánica del vuelo es una ciencia que estudia el movimiento (además de las fuerzas, momentos y energía) que realizan los vehículos aéreos a través de un medio fluido (atmósfera). Estos vehículos gobiernan sus movimientos libremente (sin ligaduras cinemáticas) en función de las deflexiones o movimientos de las superficies aerodinámicas del mismo. Además, estos vehículos voladores están sometidos a fuerzas exteriores, que son: las de origen gravitatorio (peso de la aeronave y distribución a lo largo de la geometría del avión), propulsivas (empuje de motor si lo hubiera) y aerodinámicas (generadas por las superficies sustentadoras y de control). 

Para controlar el avión (actuar en los 6 grados de libertad que tiene como sólido rígido), se puede variar la magnitud y dirección de las fuerzas aerodinámicas y propulsivas mediante las superficies de mando y palanca de gases y tobera respectivamente. Esto implica que una variación en las fuerzas (en módulo y en brazo) provoca un cambio en los momentos.

Este proyecto se centra en el estudio de las fuerzas y momentos, que aplicados en el c.g del vehículo, modifican su trayectoria y actitud. El cometido del proyecto también es, por tanto, estudiar la estabilidad y control de una aeronave, es decir, analizaremos las rotaciones del avión alrededor de su centro de gravedad como un sólido rígido y la trayectoria que describe, explicando algunos modos dinámicos típicos de una aeronave como el corto periodo.

La forma final de las ecuaciones dinámicas descritas en este proyecto se ha obtenido aplicando unas hipótesis (tierra plana, aeronave simétrica de masa constante, configuración convencional de aeronave…) y una linealización de las ecuaciones de fuerzas y momentos mediante la teoría de pequeñas perturbaciones típica en simuladores de maniobras y en la teoría de la mecánica del vuelo. La implementación de dichas ecuaciones dinámicas se ha realizado en Python 3 (lenguaje de programación libre), cuyo código se incluye en el presente proyecto.

Tal y como se ha concebido el diseño del simulador de maniobras, se tiene la capacidad de crear distintas deflexiones de mando y variación de la potencia aplicada, para luego propagar el sistema de estado del avión (velocidades en ejes cuerpo, actitud, velocidades angulares y posición) mediante un esquema de integración de derivadas parciales de Runge Kutta de 4º orden. Todo esto para una superficie alar, cuerda media aerodinámica, envergadura, masa y tensor de inercia dados. Se consigue, por tanto, el diseño conceptual de las cualidades de un avión. De manera que nuestro simulador funciona para cualquier tipo de avión con cola convencional del que se conocen superficie alar, cuerda media aerodinámica, envergadura, masa y tensor de inercia.

La realización de este proyecto se puede justificar por las grandes ventajas que proporciona, tanto a nivel educativo como profesional. Para el ámbito de aprendizaje, ayuda a entender mejor los comportamientos del avión. Si se considera aplicable o funcional para una empresa del sector aeronáutico, puede facilitar o proporcionar la posibilidad de observar el comportamiento en vuelo de una aeronave previo a su diseño final, ya que permite preveer su comportamiento en una situación real de vuelo e incluso identificar algún posible cambio en el diseño de la aeronave para una mejor funcionalidad, todo ello sin sufrir gastos en modelos ni riesgos de accidentes. 


# Hipótesis consideradas

Para poder implementar las ecuaciones de una manera más simple se han considerado diversas hipótesis de simplificación.
* 	Se asume que la masa del avión es constante, no varía. Esto se puede justificar ya que en ningún momento se especifica en que fase del vuelo se encuentra el avión (despegue, aterrizaje, comienzo del vuelo…). Además, la condición de vuelo estudiada al igual que la perturbación a la que es sometido es de un corto periodo de tiempo, en el cual las variaciones de masa se pueden considerar muy lentas.
* 	Se considera que el avión es perfectamente simétrico, algo que la realidad no es completamente cierto, ya que su diseño, fabricación y montaje no es perfecto. Pero asumimos que si y por ello I_{xy}=I_{zy}=0.
* 	Hipótesis de que la tierra es plana, lo cual tiene unos límites en distancia. Se puede asumir que la tierra es plana ya que no recorriendo más de 10 km horizontalmente no existe gran incertidumbre si miramos la localización en un GPS. Esta hipótesis simplifica mucho las ecuaciones de la mecánica del vuelo.
* 	Los momentos se podrían calcular respecto del centro aerodinámico, pero se calculan en el centro de gravedad, por lo que no hay momentos inducidos por las fuerzas gravitatorias.
* 	Se considera que el empuje del avión es paralelo a x_b, por lo que no existe calado de motor \varepsilon, ni resbalamiento \nu.
* 	Hipótesis de atmosfera en calma, es decir, no hay viento. Lo cual también simplifica las ecuaciones, además de que no es necesario simular el viento en Python.
* 	Se asume que las derivadas de estabilidad usadas para el trimado del avión son constantes para cualquier condición de vuelo, lo cual no es cierto. Las derivadas de estabilidad del avión varían en función de la condición de vuelo en la que se encuentra.

# Implementación

A partir de las ecuaciones de la mecánica del vuelo del avión descritas en el apartado 4, puede realizarse el diseño del simulador implementando estas ecuaciones en un lenguaje de programación que nos permita resolverlas. El lenguaje elegido para implementar y obtener la resolución de las ecuaciones en Python 3.

Python es un lenguaje de programación muy popular en el ámbito científico e ingenieril y supone una herramienta de gran utilidad a la hora de implementar algoritmos básicos (calculo matricial, sistemas de ecuaciones lineales) o algoritmos complejos de Deep Learning y Big Data. Para la consecución del simulador se ha hecho uso, además del lenguaje básico, de librerías libres como Numpy, Scipy y Matplotlib para acometer los cálculos y la representación gráfica de resultados respectivamente. El entorno de programación elegido es Spyder, incluido en la distribución de Python científico Anaconda (ver Ref [12])

1.  Funciones y distribución del código

Antes de poder llegar a la resolución de las ecuaciones hay que definir diferentes funciones:
*   La atmosfera ISA, a partir de la cual obtenemos T y P
* 	La densidad \rho y presión dinámica q
* 	Cp (gas isentrópico)
* 	Mach
* 	Configuración de tiempo (t_i,t_f,dt)

Definir la geometría (S,b,c)\ del F-16, así como peso (W), tensor de inercia\ \left[I\right]. 
También se han de imponer unas condiciones iniciales para poder obtener el trimado del avión, donde imponemos una altura inicial {(h}_i=30000\ ft), velocidad inicial {(v}_i=180\ kt). Además de \theta,\phi,\psi,p,q,r,\delta_e,\delta_a,\delta_r,\alpha,\beta,u,v,w,x,y.
Proponemos unas derivadas de estabilidad, que corresponden a una condición de vuelo con \alpha=\beta=0 (ver 5.6).
La implementación de las ecuaciones se ha realizado en 5 bloques:
* 	Trimador, donde se obtienen las contribuciones C_{X0},C_{Z0},C_{Y0,}C_{M0},C_{N0,}C_{l0} las cuales ya se consideran constantes en la resolución.
* 	Cambio de ejes
* 	Función que engloba todas las ecuaciones a integrar (\dot{u},\dot{v},\dot{w},\dot{\theta},\dot{\phi},\dot{\psi},\dot{p},\dot{q},\dot{r},\dot{x},\dot{y},\dot{z})
* 	Función solver (propagación del sistema dinámico con Runge Kutta 4)-
* 	Función de “inputs”, a partir de la cual se definen las maniobras del simulador (variando las variables de control del avión (\delta_e,\delta_a,\delta_r)

Finalmente existe el bloque principal, denominado “simulador de maniobras” donde se crea el bucle y se obtienen los resultados buscados.

2.  Trimado del avión

Una condición de vuelo estable con derivadas de estabilidad que son constantes o cero se denomina trimado del avión o condición de vuelo trimada. Encontrar los valores asociados a este estado y los valores de los controles, que están asociados con esta condición esutil para iniciar la simulación y definir una condición de referencia para linealizar las actuaciones. Las ecuaciones que definen el trimado son las ecuaciones de la forma \dot{x}=f(x,u)\ con las derivadas de estabilidad establecidas en un vector constante denominado c_{trim}=\ f(x,u)\ 

Esto constituye un conjunto de ecuaciones algebraicas no lineales acopladas que se pueden resolver para encontrar valores de ajuste de estado y control. Hay que tener en cuenta que las ecuaciones relacionadas con las variables de posición xE y yE no son relevantes para el análisis de dinámica y control. 

A continuación, se describe como todas las variables se suponen nulas para el trimado del avión y así poder obtener los coeficientes\ C_{X0},C_{Z0},C_{Y0,}C_{m0},C_{n0,}C_{l0}. De manera que se emplea un trimado diferente, pero a la vez muy simple. 

El avión se considera inicialmente con vuelo estacionario, rectilíneo, uniforme y con alas a nivel

De manera que para definir la condición inicial de vuelo estable:

* 	Altura inicial: h_i=30000\ ft
* 	Velocidad inicial: v_i=180\ kt
* 	\theta=\phi=\psi=0, 
* 	p=q=r=0
* 	\delta_e=\delta_a=\delta_r=0
* 	\alpha=\beta=0
* 	u=v=w=0
* 	x=y=0


En un trimado normal de avión se podrían despreciar x\ e\ y ya que sus valores solo son importantes en la navegación.

Solo se podría elegir el valor de \alpha o v_i  para la condición de vuelo inicial, ya que están relacionados.

El ángulo de resbalamiento \beta se usaría para equilibrar la asimetría lateral y la fuerza lateral de las deflexiones de las superficies de control lateral. Nuestro avión se considera simétrico. 

Los controles \delta_e,\delta_a,\delta_r equilibran los momentos aerodinámicos (considerados nulos).

La potencia motora T se considera constante e invariable durante la condición de vuelo.


3.  Secuencia de ejecución

Para conseguir los resultados obtenidos se ha seguido una secuencia de ejecución de los bloques de código en Python 3.

Tras la creación de los bloques generales, que son: la atmosfera ISA, la densidad y presión dinámica, Cp y Mach. Además de las matrices de cambios de ejes. 

A continuación, será necesario otros bloques de código específicos del avión de estudio donde debemos definir: su geometría, derivadas de estabilidad, y establecer unas condiciones iniciales.

A partir de los bloques generales y las derivadas de estabilidad, geometría y condiciones iniciales se obtiene el trimado del avión (bloque que llama a los anteriores como importaciones para obtener C_{X0},C_{Z0},C_{Y0,}C_{m0},C_{n0,}C_{l0}). 

Una vez resuelto el trimado, se puede proceder a la integración de las ecuaciones para resolverlas. De manera que debemos tener otro bloque con todas las ecuaciones dinámicas a resolver, el solver que vamos a aplicar para resolverlas (runge kutta 4), los inputs de las deflexiones y el tiempo de ejecución de la simulación. Todo esto es volcado en el bloque global (denominado simulador), en el cual obtendremos resultados en gráficas.

# Conclusiones 

Los resultados obtenidos con el simulador de maniobras descrito a lo largo de este proyecto permiten extraer las siguientes conclusiones:
Por un lado, las hipótesis planteadas se han probado correctas para cumplir el cometido de implementar un simulador de maniobras, con el foco puesto en las deflexiones de mando y la palanca de gases, que permita trasladar una serie temporal de movimiento en elevador, alerón y timón de dirección en respuestas en giros, traslaciones y gobierno del avión simulado. Estas hipótesis principalmente incluyen considerar:
* Avión de masa constante, sin pérdida de masa por consumo de combustible.
* Avión perfectamente simétrico en distribución de masas, con términos cruzados en el tensor de inercia en el plano y nulos.
* Avión de mandos convencionales, con parte longitudinal desacoplada de lateral y direccional, pero con lateral-direccional acoplado.
* Tierra plana, válida para distancias cercanas (hasta 10 km) por cometer errores en altura no mayores de 10 metros.
* Ángulos de Euler como representación de los giros entre las referencias de horizonte local y ejes cuerpo.
* Avión en vuelo estacionario antes de la ejecución de la maniobra (trimado estacionario).

Ante dobletes e impulsos en los mandos convencionales se ha observado respuestas dinámicas y modos de funcionamiento propios de un sistema dinámico con 6 grados de libertad, con lo que se puede dar por válida las hipótesis aplicadas también desde un punto de vista de cualidades de vuelo.

Las hipótesis aplicadas si bien útiles hacen que algunos aspectos de análisis del simulador sean incompletos o carezca de cierta veracidad fuera del alcance de las hipótesis. Pero a pesar de las limitaciones se puede considerar de gran utilidad ya que simula el vuelo del avión

# Bibliografía

[1]	Aerosoft (2008). F-16 Fighting Falcon X. Recuperado de: https://www.aerosoft.com/es/simulacion-de-vuelo/flight-simulator-x/aviones/1648/f-16-fighting-falcon

[2]	Cabrerizo García, F. (2014). Mecánica del Vuelo del Grado en Ingeniería Aeroespacial. Universidad Alfonso X el Sabio. Madrid.

[3]	Etkin. B. (2005). Dynamic of Atmospheric Flight. Dover Publications.

[4]	Farré. A. (2007). Controllers for Systems with Bounded Actuators: Modeling and control of an F-16 aircraft. University of California, Irvine. Recuperada de https://balsells.eng.uci.edu/files/2014/11/MSthesis_AFarre07.pdf

[5]	Gómez. M.A., Pérez. M., Puentes. C. (2012). Mecánica del vuelo. Garceta.

[6]	Huo, Y., Model of F-16 Fighter Aircraft, department of electrical engineering. University of Southern California. Los Angeles.

[7]	Kermode. A.C. (2006). Mechanics of Flight. Pearson Education Limited.

[8]	Klein. V., Morelli. E.A. (2006). Aircraft system identification: theory and practice. Reston, VA.

[9]	Nguyen, L.T., Ogburn, M.E., Gilbert, W.P., Kibler, K.S., Brown. P.W., y Deal. P.L. (1979). Simulator study of stall/post-stall characteristics of a fighter airplane with relaxed longitudinal static stability, NASA Tech. Pap. 1538. Washington, D.C.

[10]	Roskam. J. (1998). Airplane Flight Dynamics and Automatic Flight Controls. Part I & II. Darcorporation.

[11]	Tordera. M. (2012). Diseño de un simulador de helicóptero. (Proyecto fin de grado). Universidad Politécnica de Cataluña, Terrasa. Recuperada de https://upcommons.upc.edu/bitstream/handle/2099.1/16772/Memoria.pdf?sequence=1&isAllowed=y


