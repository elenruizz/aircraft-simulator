# -*- coding: utf-8 -*-
"""
Cambio de ejes:
    De body a 
"""
from numpy import array, asarray, cos, sin


def body2wind(alpha, beta, v1_w, v2_w, v3_w):
    """
    """

    v_w = asarray([v1_w, v2_w, v3_w])
    Lbw = array([ [cos(alpha)*cos(beta),   -cos(alpha)*sin(beta),      -sin(alpha)],
                    [sin(beta),              cos(beta),                  0],
                    [sin(alpha)*cos(beta),   -sin(alpha)*sin(beta),      cos(alpha)]])

    v1_b, v2_b, v3_b = Lbw@v_w
    
    return v1_b, v2_b, v3_b


def wind2body(alpha, beta, v1_b, v2_b, v3_b):
    """
    """

    v_b = asarray([v1_b, v2_b, v3_b])
    Lbw = array([ [cos(alpha)*cos(beta),   -cos(alpha)*sin(beta),      -sin(alpha)],
                    [sin(beta),              cos(beta),                  0],
                    [sin(alpha)*cos(beta),   -sin(alpha)*sin(beta),      cos(alpha)]])
    Lwb = Lbw.transpose()
    
    v1_w, v2_w, v3_w = Lwb@v_b
    
    return v1_w, v2_w, v3_w
