# -*- coding: utf-8 -*-
"""
INTENTO DE EXPORTACIONES PARA HACER TRIMADO DEL AVIÓN
"""

import sys
sys.path.insert(0, '../lib/')
sys.path.insert(0, '../scr/')
import ISA as atmos
import density_and_dynamic_pressure as Ddp
import isentropic_gas as gas
import Geometria as geo
import Condiciones_iniciales as CI

Vo=CI.vi
c = geo.c

T = atmos.T_isa(0)
print(T)
